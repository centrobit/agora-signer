from django.contrib import admin
from django.urls import path

from signer.views import AddSignInXmlFile

urlpatterns = [
    path('admin/', admin.site.urls),
    path('add-sign-in-xml/', AddSignInXmlFile.as_view(), name='add_sign_in_xml'),
]
