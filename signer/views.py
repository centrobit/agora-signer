# -*- coding: utf-8 -*-
import sys

from django.http import HttpResponse
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

from signer.settings import PATH_FOR_CADES
sys.path.append(PATH_FOR_CADES)
import pycades

@method_decorator(csrf_exempt, name='dispatch')
class AddSignInXmlFile(View):

    def get(self, request):
        try:
            store = pycades.Store()
            store.Open(pycades.CADESCOM_CONTAINER_STORE, pycades.CAPICOM_MY_STORE,
                       pycades.CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED)
            certs = store.Certificates
            return HttpResponse("Good, serts count: {}".format(certs.Count))
        except Exception as ex:
            print(ex)
            return HttpResponse(ex)

    def post(self, request):
        # Получает на вход в теле запроса xml подписывает его и возвращает в теле ответа
        try:
            store = pycades.Store()
            store.Open(pycades.CADESCOM_CONTAINER_STORE, pycades.CAPICOM_MY_STORE,
                       pycades.CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED)
            certs = store.Certificates
            signer = pycades.Signer()
            signer.Certificate = certs.Item(1)
            signer.CheckCertificate = True
            # Если у контейнера есть пароль, то он указывается тут
            signer.KeyPin = '12345678'
            
            signedXML = pycades.SignedXML()
            signedXML.Content = request.body.decode('utf-8')
            signedXML.SignatureType = pycades.CADESCOM_XML_SIGNATURE_TYPE_TEMPLATE
            signature = signedXML.Sign(signer)

            # ALFABANK_LOGIN = 'DementevSV'
            # ALFABANK_PASSWORD = '[VjqCLeA'
            # ALFABANK_API = "https://scf.alfabank.ru/API/v1/ISO20022/Statements"
            # from pip._vendor.requests.auth import HTTPBasicAuth
            # auth = HTTPBasicAuth(ALFABANK_LOGIN, ALFABANK_PASSWORD)
            # headers = {'Content-type': 'application/xml'}
            # from pip._vendor import requests
            # response = requests.post(ALFABANK_API, data=signature,headers=headers, auth=auth, verify=False)

            return HttpResponse(signature)
        except Exception as ex:
            print(ex)
            return HttpResponse(ex)
