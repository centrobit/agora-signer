FROM python:3.6

ENV TZ=Europe/Moscow

RUN mkdir /signer

COPY ./entrypoint.sh /entrypoint.sh
COPY ./requirements.txt /signer/requirements.txt
COPY ./for_install_cryptopro /for_install_cryptopro

RUN apt-get update \
 && apt-get install gettext -y \
 && apt-get install supervisor -y \
 && pip install django==3.0.8 \
 && pip install -r /signer/requirements.txt \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y locales \
 && sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
 && dpkg-reconfigure --frontend=noninteractive locales \
 && update-locale LANG=en_US.UTF-8

RUN cd for_install_cryptopro \
    && cd linux-amd64_deb \
    && ./install.sh \
    && apt install ./lsb-cprocsp-devel_5.0*.deb  \
    && cd .. \
    && cd cades_linux_amd64 \
    && apt install ./cprocsp-pki-cades*.deb \
    && cd .. \
    && /opt/cprocsp/bin/amd64/csptest -keyset -enum_cont -verifycontext -fqcn \
    && cd serts \
    && mkdir /var/opt/cprocsp/keys/root/AOrrETSr.000  \
    && cp -r ./AOrrETSr.000/. /var/opt/cprocsp/keys/root/AOrrETSr.000  \
    && /opt/cprocsp/bin/amd64/csptest -keyset -enum_cont -verifycontext -fqcn \
    && /opt/cprocsp/bin/amd64/csptest -a -c -a 

ENV LANG en_US.UTF-8 
ENV LC_ALL en_US.UTF-8

WORKDIR /signer

CMD ["./entrypoint.sh"]
